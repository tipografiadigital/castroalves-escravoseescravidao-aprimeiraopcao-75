\select@language {brazilian}
\contentsline {chapter}{Posfácio}{7}
\contentsline {section}{Sobre essa Edição}{8}
\contentsline {section}{Vida de Castro Alves}{10}
\contentsline {section}{O Movimento Abolicionista na Época de Castro Alves}{20}
\contentsline {section}{Castro Alves, Poeta Canônico}{23}
\contentsline {section}{«Lúcia»: Contra a Hipocrisia}{28}
\contentsline {section}{«O Navio"-Negreiro»: Contra a Nação}{36}
\contentsline {section}{A Cachoeira de Paulo Afonso: Contra Deus, Contra a Família e Contra a Lei}{41}
\contentsline {section}{Castro Alves, Poeta Oral}{48}
\contentsline {section}{A Importância de Castro Alves}{55}
\contentsline {section}{Bibliografia}{63}
\contentsfinish 
